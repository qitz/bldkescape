﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UniRx.Async;

namespace Qitz.AdIAPDisplaySystems
{
    public enum DialogSelect
    {
        NONE,
        POSTIVE,
        NEGATIVE,
    }
    public enum DialogDisplaySetting
    {
        NONE,
        NORMAL,
        NEGATIVE_ONLY,
        POSTIVE_ONLY,
    }

    public class AdIAPConfirmDialogFactory
    {
        public static IAdIAPConfirmDialog Create(AdIAPConfirmDialog prefab, Transform parent)
        {
            var dialog = Object.Instantiate(prefab).GetComponent<AdIAPConfirmDialog>();
            dialog.transform.SetParent(parent);
            dialog.transform.localPosition = Vector3.zero;
            dialog.transform.localScale = Vector3.one;
            return dialog;
        }
    }

    public interface IAdIAPConfirmDialog
    {
        AsyncSubject<DialogSelect> CloseSubject { get; }
        void Open(string bodyText, DialogDisplaySetting dialogDisplaySetting = DialogDisplaySetting.NORMAL);
        void ClosePostive();
        void CloseNegative();
    }

    public class AdIAPConfirmDialog : MonoBehaviour, IAdIAPConfirmDialog
    {
        [SerializeField] Text countText;
        AsyncSubject<DialogSelect> closeSubject = new AsyncSubject<DialogSelect>();
        public AsyncSubject<DialogSelect> CloseSubject => closeSubject;
        [SerializeField] Transform openAnimationBody;
        // Start is called before the first frame update
        [SerializeField] Button postiveButton;
        [SerializeField] Button negativeButton;


        void Start()
        {
            AdSystemTween.ZoomInTween(openAnimationBody, () => { });
        }
        public void Open(string bodyText, DialogDisplaySetting dialogDisplaySetting = DialogDisplaySetting.NORMAL)
        {
            countText.text = bodyText;
            if (dialogDisplaySetting == DialogDisplaySetting.NEGATIVE_ONLY)
            {
                postiveButton.gameObject.gameObject.SetActive(false);
            }
            else if (dialogDisplaySetting == DialogDisplaySetting.POSTIVE_ONLY)
            {
                negativeButton.gameObject.SetActive(false);
            }
        }

        public void ClosePostive()
        {
            AdSystemTween.ZoomOutTween(transform, () => {
                closeSubject.OnNext(DialogSelect.POSTIVE);
                closeSubject.OnCompleted();
                Destroy(gameObject);
            });
        }
        public void CloseNegative()
        {
            AdSystemTween.ZoomOutTween(transform, () => {
                closeSubject.OnNext(DialogSelect.NEGATIVE);
                closeSubject.OnCompleted();
                Destroy(gameObject);
            });
        }
    }
}