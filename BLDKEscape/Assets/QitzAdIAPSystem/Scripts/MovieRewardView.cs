﻿using System.Collections;
using System.Collections.Generic;
using Qitz.AdModules;
using UnityEngine;
using UnityEngine.UI;
using UniRx.Async;
using System;
using UniRx;

namespace Qitz.AdIAPDisplaySystems
{
    [RequireComponent(typeof(Button))]
    public class MovieRewardView : MonoBehaviour, IAdModuleReceiver
    {
        [SerializeField]
        AdIAPConfirmDialog adIAPConfirmDialogPrefab;
        [SerializeField]
        AdIAPConfirmDialog getRewadDialogPrefab;
        [SerializeField]
        string movieConfirmText = "動画をみてGitクリスタルを入手できます。\n視聴しますか？";
        [SerializeField]
        string movieRewardedText = "GitクリスタルをGet!";
        Action rewardedAction;
        DateTime nowTime= AdSysyemTimerUtil.DefaultDataTime;
        Button button => this.GetComponent<Button>();

        void Awake()
        {
            SetRewardedCallBack(()=> { });
            button.onClick.AddListener(
                ShowConfirmDialog
            );
            var saveData = AdIAPSaveDataFactory.Create();
            //一旦ボタンを押下不可にする
            button.enabled = false;
            //前回見た動画時刻を取得
            StartCoroutine(AdSysyemTimerUtil.GetNTPServerTime((dataTime)=> {
                nowTime = dataTime;
                var oneDayFutureDate = AdSysyemTimerUtil.GetOneDayFutureDateTime(saveData.LastWatchedMovieTime).Second;
                if (nowTime.Second >= oneDayFutureDate)
                {
                    button.enabled = true;
                }
            }));
        }

        public void SetRewardedCallBack(Action action)
        {
            this.rewardedAction = () => {
                action?.Invoke();
                //動画を最後に見た時間を更新する
                var saveData = AdIAPSaveDataFactory.Create();
                saveData.SetLastWatchedMovieTime(nowTime);
                button.enabled = false;
            };

        }

        async void ShowConfirmDialog()
        {
            //ダイアログを表示する
            var dialog = Instantiate(adIAPConfirmDialogPrefab).GetComponent<AdIAPConfirmDialog>();
            dialog.transform.SetParent(this.transform.parent);
            dialog.transform.localPosition = Vector3.zero;
            dialog.transform.localScale = Vector3.one;
            dialog.Open(movieConfirmText);
            DialogSelect selectResult = await dialog.CloseSubject;
            if(selectResult == DialogSelect.POSTIVE)
            {
                this.GetQitzAdModule().SetRewardedCallBack(()=> {
                    this.rewardedAction.Invoke();
                    //ダイアログを表示する
                    var _dialog = Instantiate(getRewadDialogPrefab).GetComponent<AdIAPConfirmDialog>();
                    _dialog.transform.SetParent(this.transform.parent);
                    _dialog.transform.localPosition = Vector3.zero;
                    _dialog.transform.localScale = Vector3.one;
                    _dialog.Open(movieRewardedText, DialogDisplaySetting.POSTIVE_ONLY);

                });
                this.GetQitzAdModule().ShowReword();

            }
        }
    }
}