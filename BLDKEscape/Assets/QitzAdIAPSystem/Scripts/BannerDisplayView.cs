﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qitz.AdModules;

namespace Qitz.AdIAPDisplaySystems
{
    public class BannerDisplayView : MonoBehaviour, IAdModuleReceiver
    {

        // Start is called before the first frame update
        void Start()
        {
            var iapDataStore = AdIAPSaveDataFactory.Create();
            if (iapDataStore.IsEnableAD)
            {
                this.GetQitzAdModule().ShowBanner();
            }

        }
        void OnDestroy()
        {
            this.GetQitzAdModule().DestroyBanner();
        }

    }
}