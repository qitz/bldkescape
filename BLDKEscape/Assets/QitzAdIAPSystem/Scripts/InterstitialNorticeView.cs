﻿using System.Collections;
using System.Collections.Generic;
using Qitz.AdModules;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;
using Qitz.AdIAPDisplaySystems;

namespace Qitz.AdIAPDisplaySystems
{
    public class InterstitialNorticeView : MonoBehaviour, IAdModuleReceiver
    {
        [SerializeField] Text countText;
        AsyncSubject<Unit> closeSubject = new AsyncSubject<Unit>();
        public AsyncSubject<Unit> CloseSubject => closeSubject;
        [SerializeField] Transform openAnimationBody;
        // Start is called before the first frame update
        [SerializeField] Button postiveButton;


        public async void Open()
        {
            //countText.text = bodyText;
            AdSystemTween.ZoomInTween(openAnimationBody, () => { });
            await UniTask.Delay(2000);
            this.GetQitzAdModule().ShowInterstitial();
            ClosePostive();
        }

        void ClosePostive()
        {
            AdSystemTween.ZoomOutTween(transform, () => {
                closeSubject.OnNext(Unit.Default);
                closeSubject.OnCompleted();
                Destroy(gameObject);
            });
        }

    }
}