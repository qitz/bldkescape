﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using System;

namespace Qitz.AdIAPDisplaySystems
{
    public class IAPBuyButton : MonoBehaviour
    {
        PurchaseIntializer.ProductElement productInfo;
        [SerializeField]
        Button button;
        [SerializeField]
        Text description;
        [SerializeField]
        Text title;
        [SerializeField]
        Text price;

        public void Initialize(PurchaseIntializer.ProductElement productInfo,Action<PurchaseIntializer.ProductElement> buyAction)
        {
            this.productInfo = productInfo;
            button.onClick.AddListener(()=> {
                buyAction.Invoke(this.productInfo);
            });
            description.text = productInfo.Product.metadata.localizedDescription;
            title.text = productInfo.Product.metadata.localizedTitle;
            price.text = productInfo.Product.metadata.localizedPriceString;
        }

    }
}