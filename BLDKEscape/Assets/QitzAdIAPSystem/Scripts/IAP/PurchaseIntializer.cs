﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UniRx;
using UniRx.Triggers;
using UniRx.Async;
using System.Linq;

namespace Qitz.AdIAPDisplaySystems
{
    // Purchaser クラスは IStoreListener から派生させると Unity Purchasing からのメッセージを受領できるようになります。
    public class PurchaseIntializer : MonoBehaviour, IStoreListener
    {

        [SerializeField]
        List<ProductElement> productElements;
        List<ProductElement> selectedPlatformProdducts => productElements.Where(pe => pe.PlatformType == Application.platform).ToList();
        [SerializeField]
        LoadingScreenView loadingScreenView;
        [SerializeField]
        AdIAPConfirmDialog adIAPConfirmDialogPrefab;
        [SerializeField]
        Transform dialogParent;
        [SerializeField]
        string purchaseSuccessMessage = "購入完了しました。";
        [SerializeField]
        string purchaseFailedMessage = "購入できませんでした。";
        [SerializeField]
        GameObject productDisplay;

        public bool IsLoadedProducts
        {
            get;
            private set;
        }

        void Start()
        {
            Initialize();
        }

        /// <summary>
        /// 初期化メソッド
        /// </summary>
        void Initialize()
        {
            var module = StandardPurchasingModule.Instance();
#if UNITY_EDITOR
            module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser; //Unityエディタでのダミー表示
#endif
            var builder = ConfigurationBuilder.Instance(module);

            selectedPlatformProdducts.ForEach(pe =>
            {
                builder.AddProduct(pe.ProductID,pe.ProductType);
            });

            //第一引数にIStoreListenerを継承したクラス
            UnityPurchasing.Initialize(this, builder);
            //ローディングスクリーン表示
            loadingScreenView.Open();
            productDisplay.SetActive(false);
            this.UpdateAsObservable().Where(_ => IsLoadedProducts).Take(1).Subscribe(_ =>
            {
                //ローディングスクリーンを解除
                loadingScreenView.Close();
                productDisplay.SetActive(true);
                //ButtonViewの更新
                selectedPlatformProdducts.ForEach(pe =>
                {
                    var product = Controller.products.WithID(pe.ProductID);
                    pe.SetProduct(product);
                    pe.LinkedIAPBuyButton.Initialize(pe, Purchase);

                });

            }).AddTo(this.gameObject);

        }


        /// <summary>
        /// 購入
        /// </summary>
        /// <param name="ProductID">製品ID</param>
        void Purchase(ProductElement productElement)
        {
            loadingScreenView.Open();
            Controller.InitiatePurchase(productElement.Product);
            //成功時に課金通貨を増やす
            purchaseSuccessSubject.Take(1).Subscribe(_ =>
            {
                if(productElement.ProductType == ProductType.Consumable)
                {
                    var iapDataStore = AdIAPSaveDataFactory.Create();
                    iapDataStore.AddMoney(productElement.IncreseValue);
                }
                var dialog = AdIAPConfirmDialogFactory.Create(adIAPConfirmDialogPrefab,dialogParent);
                dialog.Open(purchaseSuccessMessage, DialogDisplaySetting.POSTIVE_ONLY);

            }).AddTo(this.gameObject);

            //失敗時に失敗ダイアログを表示する
            PurchaseFailedSubject.Take(1).Subscribe(_ =>
            {
                var dialog = AdIAPConfirmDialogFactory.Create(adIAPConfirmDialogPrefab, dialogParent);
                dialog.Open(purchaseFailedMessage, DialogDisplaySetting.POSTIVE_ONLY);
            }).AddTo(this.gameObject);

        }

        IStoreController Controller { get; set; }
        IExtensionProvider Extensions { get; set; }

        /// <summary>
        /// IAP初期化完了コールバック
        /// </summary>
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            Debug.Log("IAPの初期化が完了しました");
            IsLoadedProducts = true;
            Controller = controller;
            Extensions = extensions;
        }


        /// <summary>
        /// IAP初期化失敗コールバック
        /// </summary>
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("IAP初期化失敗コールバック="+error.ToString());
        }

        Subject<PurchaseFailureReason> purchaseFailedSubject = new Subject<PurchaseFailureReason>();
        public Subject<PurchaseFailureReason> PurchaseFailedSubject
        {
            get
            {
                return purchaseFailedSubject;
            }
        }
        /// <summary>
        /// 購入失敗コールバック
        /// </summary>
        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
            loadingScreenView.Close();
            PurchaseFailedSubject.OnNext(p);
            Debug.Log("購入失敗コールバック");
        }

        Subject<PurchaseEventArgs> purchaseSuccessSubject = new Subject<PurchaseEventArgs>();
        public Subject<PurchaseEventArgs> PurchaseSuccessSubject{
            get{
                return purchaseSuccessSubject;
            }
        }
        /// <summary>
        /// 購入成功コールバック
        /// </summary>
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            Debug.Log("購入成功コールバック");
            loadingScreenView.Close();
            purchaseSuccessSubject.OnNext(e);
            return PurchaseProcessingResult.Complete;
            //Firebaseの購入時完了処理にデータを食わせてやるべし
        }


        [Serializable]
        public class ProductElement
        {
            public RuntimePlatform PlatformType;
            public ProductType ProductType;
            public string ProductID;
            public IAPBuyButton LinkedIAPBuyButton;
            public int IncreseValue;
            Product product;
            public Product Product => product;
            public void SetProduct(Product product)
            {
                this.product = product;
            }

        }

    }
}