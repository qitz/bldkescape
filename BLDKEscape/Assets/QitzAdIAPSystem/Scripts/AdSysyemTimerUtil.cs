﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
namespace Qitz.AdIAPDisplaySystems
{
    public sealed class AdSysyemTimerUtil
    {
        public const double OneDayForSecond = 60 * 60 * 24;

        static readonly string NTP_URL = "https://ntp-a1.nict.go.jp/cgi-bin/json";

        /// <summary>
        /// サーバー時刻を取得します
        /// </summary>
        /// <returns>The NTPS erver time.</returns>
        /// <param name="callback">Callback.</param>
        public static IEnumerator GetNTPServerTime(Action<DateTime> callback)
        {
            var req = UnityWebRequest.Get(NTP_URL);

            yield return req.SendWebRequest();

            if (req.isNetworkError)
            {
                Debug.LogError(req.error);
            }
            else
            {
                var response = JsonUtility.FromJson<NTPServerInfo>(req.downloadHandler.text);
                callback(response.DateTime);
            }
        }
        [Serializable]
        class NTPServerInfo
        {
            public string id;
            public double it;
            public double st;
            public int leap;
            public long next;
            public int step;
            public double TimeStamp
            {
                get
                {
                    return st;
                }
            }
            public DateTime DateTime
            {
                get
                {
                    var startDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    var unixDate = startDate.AddSeconds(st);
                    return unixDate.ToLocalTime();
                }
            }

        }
        public const double ALLOWABLE_DIFF_TIME_LOCAL_TIME_FROM_SERVER_TIME = 60 * 20;
        public static string FroatToMinAndSeconds(float time)
        {
            int min = (int)(time / 60);
            int second = (int)(time % 60);
            return min.ToString("00") + ":" + second.ToString("00");
        }
        public static string FroatToMin(float time)
        {
            int min = (int)(time / 60) % 60;
            //int second = (int)(time%60);
            return min.ToString();
        }
        public static string FroatToHour(float time)
        {
            int hour = (int)(time / (60 * 60));
            //int second = (int)(time%60);
            return "" + hour;
        }

        public static string FroatToMinLowerFigureCuted(float time)
        {
            int min = (int)(time / 60);
            int second = (int)(time % 60);
            return min.ToString("00") + ":" + second.ToString("00");
        }

        public static string FroatToMinOnly(float time)
        {
            int min = (int)(time / 60);
            //int second = (int)(time % 60);
            return min.ToString("00") + ":" + "00";
        }

        public static string FroatToHourAndMinite(float time)
        {
            int min = (int)(time / 60) % 60;
            int hour = (int)(time / (60 * 60));
            return hour + "<size=60>h</size>" + min + "<size=60>m</size>";
        }

        public static int FromToMinVal(float time)
        {
            return (int)(time / 60);
        }


        //public double CurrentTimeStamp(DateTime )
        //{
        //    get
        //    {
        //        return (DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        //    }
        //}

        public static DateTime GetLocalDataTimeFromTimeStamp(double timeStamp)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(timeStamp).ToLocalTime();
        }
        public static DateTime DefaultDataTime => new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static double GetTimeStampFromDataTime(DateTime dateTime)
        {
            return (dateTime.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }
        public static DateTime GetOneDayFutureDateTime(DateTime dateTime)
        {
            return dateTime.AddSeconds(OneDayForSecond).ToLocalTime();
        }

        //public DateTime GetFutureDateTime(double addTime)
        //{
        //    return NowDataTime.AddSeconds(addTime).ToLocalTime();
        //}
    }
}