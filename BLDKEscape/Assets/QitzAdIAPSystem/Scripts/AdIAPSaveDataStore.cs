﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qitz.AdIAPDisplaySystems
{

    public interface IAdIAPSaveDataStore
    {
        DateTime LastWatchedMovieTime { get; }
        int Money { get; }
        void DecreaseMoney(int value);
        void AddMoney(int money);
        bool IsEnableAD { get; }
        void SetADEnable(bool active);
        void SetLastWatchedMovieTime(DateTime dateTime);
    }

    public class AdIAPSaveDataFactory
    {
        static IAdIAPSaveDataStore adIAPSaveDataStore;
        public static IAdIAPSaveDataStore Create()
        {
            //キャッシュして参照を渡すようにする。
            adIAPSaveDataStore = new AdIAPSaveDataStore();
            return adIAPSaveDataStore;
        }
    }

    public class AdIAPSaveDataStore : IAdIAPSaveDataStore
    {
        const string SAVE_KEY = "QitzAdSystemSaveData";
        ADSaveData saveData;

        public AdIAPSaveDataStore()
        {
            var json = PlayerPrefs.GetString(SAVE_KEY, "");
            if (json == "")
            {
                saveData = new ADSaveData();
            }
            else
            {
                saveData = JsonUtility.FromJson<ADSaveData>(json);
            }
        }

        public DateTime LastWatchedMovieTime { get { 
                if(saveData == null)
                {
                    return AdSysyemTimerUtil.DefaultDataTime;
                }
                else
                {
                    return AdSysyemTimerUtil.GetLocalDataTimeFromTimeStamp(saveData.LastWatchedMovieTime);
                }

            }
        }

        public bool IsEnableAD => saveData.isEnableAD;

        public int Money => saveData.Money;

        public void AddMoney(int money)
        {
            saveData.Money += money;
            Save();
        }

        public void DecreaseMoney(int value)
        {
            saveData.Money -= value;
            Save();
        }

        public void SetADEnable(bool active)
        {
            saveData.isEnableAD = active;
            Save();
        }

        public void SetLastWatchedMovieTime(DateTime dateTime)
        {
            saveData.LastWatchedMovieTime = AdSysyemTimerUtil.GetTimeStampFromDataTime(dateTime);
            Save();
        }
        void Save()
        {
            PlayerPrefs.SetString(SAVE_KEY, saveData.ToJson());
            PlayerPrefs.Save();
        }

        [Serializable]
        class ADSaveData
        {
            public double LastWatchedMovieTime=0;
            public bool isEnableAD = true;
            public int Money = 0;
            public string ToJson()
            {
                return JsonUtility.ToJson(this);
            }
        }

    }
}