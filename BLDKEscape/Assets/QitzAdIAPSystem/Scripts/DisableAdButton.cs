﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Async;
using UnityEngine.UI;

namespace Qitz.AdIAPDisplaySystems
{
    [RequireComponent(typeof(Button))]
    public class DisableAdButton : MonoBehaviour
    {
        [SerializeField]
        int purchasePrice=100;
        [SerializeField]
        AdIAPConfirmDialog adIAPConfirmDialogPrefab;
        [SerializeField]
        Transform dialogParent;
        [SerializeField]
        Text priceText;
        Button button => this.GetComponent<Button>();
        [SerializeField]
        Image diableImage;

        void SetDisable()
        {
            diableImage.gameObject.SetActive(true);
            button.interactable = false;
        }

        async void PurchaseDisableAd()
        {
            var iapDataStore = AdIAPSaveDataFactory.Create();
            bool lockAbleAdDisplay = purchasePrice <= iapDataStore.Money;
            if (lockAbleAdDisplay)
            {
                //100Gitクリスタルを消費して広告を非表示にします。ダイアログ
                var dialog = CreateDialog($"{purchasePrice}Gitクリスタルを消費して広告を非表示にします。");
                var select = await dialog.CloseSubject;
                if(select == DialogSelect.POSTIVE)
                {
                    iapDataStore.DecreaseMoney(purchasePrice);
                    iapDataStore.SetADEnable(false);
                    SetDisable();
                    CreateDialog($"広告が非表示になりました。", DialogDisplaySetting.POSTIVE_ONLY);
                }
            }
            else
            {
                //お金が足りませんDialogを出す
                var dialog = CreateDialog($"Gitクリスタルが足りません。", DialogDisplaySetting.POSTIVE_ONLY);
            }
        }
        AdIAPConfirmDialog CreateDialog(string text,DialogDisplaySetting displaySetting = DialogDisplaySetting.NORMAL)
        {
            var dialog = Instantiate(adIAPConfirmDialogPrefab).GetComponent<AdIAPConfirmDialog>();
            dialog.transform.SetParent(dialogParent);
            dialog.transform.localPosition = Vector3.zero;
            dialog.transform.localScale = Vector3.one;
            dialog.Open(text);
            return dialog;
        }

        // Start is called before the first frame update
        void Start()
        {
            diableImage.gameObject.SetActive(false);
            var iapDataStore = AdIAPSaveDataFactory.Create();
            if (!iapDataStore.IsEnableAD)
            {
                SetDisable();
            }
            priceText.text = "" + purchasePrice;
            button.onClick.AddListener(PurchaseDisableAd);
        }
    }
}