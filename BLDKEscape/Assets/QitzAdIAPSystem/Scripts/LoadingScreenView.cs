﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Qitz.AdIAPDisplaySystems
{
    public class LoadingScreenView : MonoBehaviour
    {
        [SerializeField]
        Image loadingImage;
        [SerializeField]
        Sprite[] loadingSprits;
        int currentSpriteCount = 0;

        // Start is called before the first frame update
        void Start()
        {

        }

        public void Open()
        {
            this.gameObject.SetActive(true);
        }
        public void Close()
        {
            this.gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            loadingImage.sprite = loadingSprits[currentSpriteCount % loadingSprits.Length];
            currentSpriteCount++;
        }
    }
}
