﻿using Qitz.ArchitectureCore;

namespace Qitz.BLDKEscape
{
    public class QuestGetTransmitter : BaseTransmitter
    {
        public static IQuestVOsGetUseCase Create(IOutGameDataStore outGameDataStore)
        {
            return new QuestVOsGetUseCase(outGameDataStore);
        }
    }

}
