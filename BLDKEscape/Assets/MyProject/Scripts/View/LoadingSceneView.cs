﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingSceneView : MonoBehaviour, IOpenable, IClosable
{
    [SerializeField] Transform tweenTarget;

    void Start()
    {
        Open();
    }

    public void Open()
    {
        MyTweener.ZoomInTween(tweenTarget, () => { });
    }

    public void Close()
    {
        MyTweener.ZoomOutTween(tweenTarget, () => { });
    }

}
