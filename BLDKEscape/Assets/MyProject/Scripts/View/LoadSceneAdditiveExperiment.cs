﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneAdditiveExperiment : MonoBehaviour
{
    void Start()
    {
        SceneManager.LoadSceneAsync("15_Loading", LoadSceneMode.Additive);

    }

}
