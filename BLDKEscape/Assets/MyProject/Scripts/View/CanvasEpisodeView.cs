﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Qitz.ArchitectureCore;
using Qitz.OuteGame;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

namespace Qitz.BLDKEscape
{
    public enum EpisodeCanvasState
    {
        Main,
        EpisodeSelect,

    }

    public class CanvasEpisodeView : MonoBehaviour, IView
    {
        public Transform continueButtonParent;
        public Button continueButton;
        public Button episodeSelectButton;

        public Transform episodeButtonParent;
        public Transform episodeButtonPrefab;

        ReactiveProperty<EpisodeCanvasState> canvasState = new ReactiveProperty<EpisodeCanvasState>();
        List<Transform> episodeButtons = new List<Transform>();

        void Start()
        {
            canvasState
                .Subscribe(s =>
                {
                    continueButtonParent.gameObject.SetActive(s == EpisodeCanvasState.Main);
                    episodeButtonParent.gameObject.SetActive(s == EpisodeCanvasState.EpisodeSelect);
                })
                .AddTo(this);

            canvasState
                .Where(s => s == EpisodeCanvasState.EpisodeSelect)
                .Subscribe(_ =>
                {
                    for(int i = 0; i < episodeButtons.Count; i++)
                    {
                        int delay = i;
                        episodeButtons[i].GetComponentsInChildren<Graphic>().ToList()
                            .ForEach(g =>
                            {
                                var to = g.transform.localPosition;
                                var from = to - Vector3.up * 150;
                                g.transform.localPosition = from;

                                g.transform
                                    .DOLocalMove(to, 0.75f)
                                    .SetDelay(i * 0.3f);

                                var c = g.color;
                                c.a = 0;
                                g.color = c;
                                DOTween.ToAlpha(
                                    () => g.color,
                                    color => g.color = color,
                                    1f,
                                    0.75f
                                )
                                .SetDelay(i * 0.3f);
                            });

                    }
                }).AddTo(this);

            episodeSelectButton.OnPointerClickAsObservable()
                .Subscribe(_ => canvasState.SetValueAndForceNotify(EpisodeCanvasState.EpisodeSelect))
                .AddTo(this);

            Observable.Timer(TimeSpan.FromSeconds(1f))
                .Subscribe(_ =>
                {
                    var vos = this.GetController<OutGameController>().GetQuestVOs();

                    for (int i = 0; i < vos.Count; i++)
                    {
                        var newButton = Instantiate(episodeButtonPrefab);
                        newButton.localPosition = Vector3.one;
                        newButton.SetParent(episodeButtonParent);
                        newButton.transform.localScale = Vector3.one;
                        newButton.GetComponentInChildren<Text>().text = vos[i].Title;

                        episodeButtons.Add(newButton);

                    }

                })
                .AddTo(this);


        }

        void ShowEpisodeButtons()
        {
            episodeButtonParent.gameObject.SetActive(true);
        }

        void HideEpisodeButtons()
        {
            episodeButtonParent.gameObject.SetActive(false);

        }

    }

}
