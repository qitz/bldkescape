﻿using System;
using UniRx;    
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoaderComponent : MonoBehaviour
{
    [SerializeField] string sceneName;
    [SerializeField] bool isLoadSceneOnStart;
    [SerializeField] float delay;

    bool isLoading;

    void Start()
    {
        if (isLoadSceneOnStart)
        {
            LoadSceneAsync();

        }

    }

    public void LoadSceneAsync()
    {
        if (isLoading)
            return;

        isLoading = true;

        var ope = SceneManager.LoadSceneAsync(sceneName);

        ope.allowSceneActivation = false;

        Observable.Timer(TimeSpan.FromSeconds(delay))
            .Subscribe(_2 => { ope.allowSceneActivation = true; });

        }

}
