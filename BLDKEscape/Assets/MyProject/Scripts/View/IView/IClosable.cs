﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IClosable
{
    void Close();

}
