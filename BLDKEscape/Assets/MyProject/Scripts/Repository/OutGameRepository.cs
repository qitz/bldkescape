﻿using Qitz.ArchitectureCore;
using UnityEngine;

namespace Qitz.BLDKEscape
{
    public interface IOutGameRepository
    {
        OutGameDataStore DataStore { get; }

    }

    [CreateAssetMenu]
    public class OutGameRepository : ARepository, IOutGameRepository
    {
        [SerializeField] OutGameDataStore dataStore;
        public OutGameDataStore DataStore { get { return dataStore; } }
    }

}
