﻿using System.Collections.Generic;

namespace Qitz.BLDKEscape
{
    public interface IQuestVOsGetUseCase
    {
        List<IQuestVO> GetQuestVOs();

    }

}
