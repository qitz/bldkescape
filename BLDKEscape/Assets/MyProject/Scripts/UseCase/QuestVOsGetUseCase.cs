﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qitz.BLDKEscape
{
    public class QuestVOsGetUseCase : IQuestVOsGetUseCase
    {
        IOutGameDataStore outGameDataStore;
        public QuestVOsGetUseCase(IOutGameDataStore outGameDataStore)
        {
            this.outGameDataStore = outGameDataStore;
        }

        public List<IQuestVO> GetQuestVOs()
        {
            return outGameDataStore.QuestList;
        }

    }
}