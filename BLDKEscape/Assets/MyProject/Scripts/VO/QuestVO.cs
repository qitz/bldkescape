﻿using UnityEngine;

namespace Qitz.BLDKEscape
{
    [System.Serializable]
    public class QuestVO : IQuestVO
    {
        [SerializeField] int id;
        [SerializeField] string title;

        public int ID => id;
        public string Title => title;

    }

}
    