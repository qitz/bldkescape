﻿using Qitz.ArchitectureCore;

namespace Qitz.BLDKEscape
{
    public interface IQuestVO : IVO
    {
        int ID { get; }
        string Title { get; }

    }

}
