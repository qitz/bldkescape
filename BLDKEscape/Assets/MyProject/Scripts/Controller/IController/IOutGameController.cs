﻿using System.Collections;
using System.Collections.Generic;
using Qitz.ArchitectureCore;
using Qitz.BLDKEscape;
using UnityEngine;

namespace Qitz.BLDKEscape
{
    public interface IOutGameController : IController
    {
        List<IQuestVO> GetQuestVOs();

    }

}

