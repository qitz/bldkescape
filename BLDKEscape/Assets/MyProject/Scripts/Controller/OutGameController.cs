﻿
using UnityEngine;
using Qitz.ArchitectureCore;
using Qitz.BLDKEscape;
using System.Collections.Generic;
using UniRx;
using UniRx.Async;
using Qitz.AdModules;
using Qitz.ADVGame;

namespace Qitz.OuteGame
{
    public class OutGameController : AController<OutGameRepository>, IOutGameController, IAdModuleReceiver
    {
        [SerializeField]
        OutGameRepository repository;

        protected override OutGameRepository Repository { get { return repository; } }

        public List<IQuestVO> GetQuestVOs()
        {
            return QuestGetTransmitter.Create(repository.DataStore).GetQuestVOs();
        }

        private async void Awake()
        {
            var data = await repository.DataStore.Initialize();
            Debug.Log(data.Count);
        }
        void Start()
        {
            this.GetQitzAdModule().ShowBanner();
            FindObjectOfType<ADVGameController>().SetTagAction(ADVTagAction);
        }
        void ADVTagAction(string value)
        {
            if(value == ActionTag.cm.ToString())
            {
                Debug.Log("ShowCM");
                this.GetQitzAdModule().ShowInterstitial();
            }
        }

    }
}