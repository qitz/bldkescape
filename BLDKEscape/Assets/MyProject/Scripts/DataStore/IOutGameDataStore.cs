﻿using System.Collections.Generic;
using Qitz.BLDKEscape;

namespace Qitz.BLDKEscape
{
    public interface IOutGameDataStore
    {
        List<IQuestVO> QuestList { get; }
        UniRx.Async.UniTask<List<IQuestVO>> Initialize();
    }
}