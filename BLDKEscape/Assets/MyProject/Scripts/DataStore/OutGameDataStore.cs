﻿using Qitz.ArchitectureCore;
using UnityEngine;
using Qitz.DataUtil;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Async;
using System.Linq;

namespace Qitz.BLDKEscape
{
    [CreateAssetMenu]
    public class OutGameDataStore : ADataStore, IOutGameDataStore
    {
        [SerializeField] TextAsset questTextAsset;

        List<IQuestVO> questList;
        public List<IQuestVO> QuestList => questList;

        public async UniTask<List<IQuestVO>> Initialize()
        {
            await JsonFromGoogleSpreadSheet.GetTeargetTypeDataFromGoogleSpreadSheetUrl<QuestVO>(
                "https://docs.google.com/spreadsheets/d/1pp6UEumKV_lbyO1k0sxQHTEUU5s1cBSR7j6s6V2JeRE/edit#gid=1122601730",
                (result) => { questList = result.Select(r => (IQuestVO)r).ToList(); });
            return questList;

        }
    }



}
