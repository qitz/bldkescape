﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

namespace Qitz.EscapeFramework
{
    public class ItemDropable : MonoBehaviour
    {
        Action<string> dropAction;
        public Action<string> DropAction => dropAction;

        public void SetDropAction(Action<string> dropAction)
        {
            this.dropAction += dropAction;
        }

    }
}
