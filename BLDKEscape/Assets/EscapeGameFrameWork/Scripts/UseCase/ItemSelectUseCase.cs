﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Qitz.EscapeFramework
{
    public interface IItemSelectUseCase
    {
        string SelectedItem { get; }
        void SelectItem(string item);
        IItemWindowView ItemWindowView { get; }
        void DisSelectItem();
    }

    public class ItemSelectUseCase: IItemSelectUseCase
    {
        string selectedItem;
        public string SelectedItem => selectedItem;
        ItemWindowView itemWindowView;
        public IItemWindowView ItemWindowView => itemWindowView;

        public ItemSelectUseCase(ItemWindowView itemWindowView)
        {
            this.itemWindowView = itemWindowView;
        }

        public void SelectItem(string item)
        {
            if (selectedItem == item)
            {
                UseItem(item);
            }
            selectedItem = item;
        }

        public void DisSelectItem()
        {
            selectedItem = "";
            itemWindowView.DisSelectDisplay();
        }

        void UseItem(string item)
        {
            ItemWindowView.UseItem(item);
            Debug.Log("UseItem");
        }
    }
}