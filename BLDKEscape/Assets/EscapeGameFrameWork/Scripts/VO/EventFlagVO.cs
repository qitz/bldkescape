﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qitz.EscapeFramework
{
    public interface IEventFlagVO
    {
        string EventType { get; }
        bool IsOn { get; }
    }

    [System.Serializable]
    public class EventFlagVO: IEventFlagVO
    {

        [SerializeField]
        string eventType;
        [SerializeField]
        bool isOn;

        public string EventType => eventType;
        public bool IsOn => isOn;

        public EventFlagVO(string eventType,bool isOn)
        {
            this.eventType = eventType;
            this.isOn = isOn;
        }

    }
}