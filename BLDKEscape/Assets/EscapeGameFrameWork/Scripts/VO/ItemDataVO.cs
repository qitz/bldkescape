﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Qitz.EscapeFramework {

    public interface IItemDataVO
    {
        string ItemName { get; }
        Sprite Sprite { get; }
        string ItemDescription { get; }
        ItemCompostionInfo[] ItemCompostionInfos { get; }
        string JugSynthesizeable(string itemA, string itemB);
    }
    [System.Serializable]
    public class ItemCompostionInfo
    {
        [SerializeField]
        string itemA;
        public string ItemA => itemA;
        [SerializeField]
        string itemB;
        public string ItemB => itemB;

        public bool JugSynthesizeable(string itemA, string itemB)
        {
            List<string> allItem = new List<string>() {this.itemA,this.itemB};
            return allItem.Contains(itemA) && allItem.Contains(itemB);
        }

    }

    [System.Serializable]
    public class ItemDataVO: IItemDataVO
    {
        [SerializeField]
        string itemName;
        public string ItemName => itemName;

        [SerializeField]
        Sprite sprite;
        public Sprite Sprite => sprite;

        [SerializeField]
        string itemDescription;

        [SerializeField]
        ItemCompostionInfo[] itemCompostionInfos;
        public ItemCompostionInfo[] ItemCompostionInfos => itemCompostionInfos;

        public string ItemDescription => itemDescription;
        public string JugSynthesizeable(string itemA, string itemB)
        {
            foreach (var ic in itemCompostionInfos)
            {
                bool synthesizeable=ic.JugSynthesizeable(itemA, itemB);
                if (synthesizeable)
                {
                    return itemName;
                }
            }
            return "";
        }

    }
}
