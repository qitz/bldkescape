﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qitz.EscapeFramework
{
    public interface ICountEventVO
    {
        int Count { get; }
        string CountEventType { get; }
        void IncrementCount();
        void DecrementCount();
        void SetDefaulCount();
    }

    [System.Serializable]
    public class CountEventVO:ICountEventVO
    {
        [SerializeField]
        string countEventType;
        public string CountEventType => countEventType;
        [SerializeField,HideInInspector]
        int count = 0;
        public int Count => count;

        public CountEventVO(string countEventType)
        {
            this.countEventType = countEventType;
        }

        public void IncrementCount()
        {
            count++;
        }

        public void DecrementCount()
        {
            count--;
        }

        public void SetDefaulCount()
        {
            count = 0;
        }
        public void IncrementCount(int _count)
        {
            count+= _count;
        }

        public void DecrementCount(int _count)
        {
            count-= _count;
        }

        public void SetCount(int _count)
        {
            count = _count;
        }
    }

    //[System.Serializable]
    //public class CountEventSetting: ICountEventSetting
    //{
    //    [SerializeField]
    //    CountEventType countEventType;
    //    [SerializeField]
    //    int maxCount = 1;
    //    [SerializeField]
    //    int minCount = 0;

    //    public CountEventType CountEventType => countEventType;

    //    public int MaxCount => maxCount;

    //    public int MinCount => minCount;
    //}

    //public interface ICountEventSetting
    //{
    //    CountEventType CountEventType { get; }
    //    int MaxCount { get; }
    //    int MinCount { get; }
    //}


}
