﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemVO
{
    string ItemName { get; }
}

[System.Serializable]
public class ItemVO : IItemVO
{
    [SerializeField]
    string itemName;
    public string ItemName => itemName;
    public ItemVO(string itemName)
    {
        this.itemName = itemName;
    }
}
