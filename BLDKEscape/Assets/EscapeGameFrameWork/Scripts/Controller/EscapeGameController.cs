﻿
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;
using System.Collections;
using UniRx.Async;
using Qitz.ADVGame;

namespace Qitz.EscapeFramework
{
    //public enum SynthesizeResult
    //{
    //    NONE,
    //    SUCCESS,
    //    FAILED,
    //}

    public interface IEscapeGameController
    {
        void AddEventExecuteCallBack(Action<AEvent[]> addEventExecuteCallBack);
        void AddUserItemListChangeCallBack(Action<List<IItemDataVO>> addUserItemListChangeCallBack);
        IEscapeGameDefinsDataStore GetEscapeGameDefins();
        bool GetItemPossession(string itemName);
        int GetNumberEventValue(string eventName);
        void GetItem(string itemName);
        void LostItem(string itemName);
        void IncreaseCountEvent(string eventName,int count);
        void DecreaseCountEvent(string eventName, int count);
        void SetCountEvent(string eventName, int count);
    }

    public class EscapeGameController : AController<EscapeGameRepository>, IEscapeGameController
    {
        [SerializeField]
        EscapeGameRepository repository;
        protected override EscapeGameRepository Repository { get { return repository; } }
        Action<AEvent[]> eventExecuteCallBack;
        Action<List<IItemDataVO>> userItemListChangeCallBack;
        IExcuteEventUseCase excuteEventUseCase;
        [SerializeField]
        EscapeGameAudioPlayer escapeGameAudioPlayer;
        [SerializeField]
        ADVWindowView aDVWindowView;
        [SerializeField]
        ScreenEffectView screenEffectView;
        [SerializeField]
        ItemWindowView itemWindowView;
        string selectedItem;
        ItemSelectUseCase itemSelectUseCase;

        public void AddEventExecuteCallBack(Action<AEvent[]> addEventExecuteCallBack)
        {
            eventExecuteCallBack += addEventExecuteCallBack;
        }

        public void AddUserItemListChangeCallBack(Action<List<IItemDataVO>> addUserItemListChangeCallBack)
        {
            userItemListChangeCallBack += addUserItemListChangeCallBack;
        }

        void Awake()
        {
            aDVWindowView.Close();
            repository.Initialize();
            itemSelectUseCase = new ItemSelectUseCase(itemWindowView);
            itemWindowView.Hide();
            var gameEventExecutorUseCase = new GameEventExecutorUseCase(Repository.EscapeGameUserDataStore, escapeGameAudioPlayer, aDVWindowView, screenEffectView, itemSelectUseCase);

            excuteEventUseCase = new ExcuteGameEventUseCase(
                (events) => {
                    //イベント実行後のコールバック
                    //ユーザーデータのアイテム数をItemViewに反映させる処理など
                    eventExecuteCallBack?.Invoke(events);
                    userItemListChangeCallBack?.Invoke(repository.UserPossessionItemSpriteList);
                }
                    , gameEventExecutorUseCase
                    , itemSelectUseCase
                    );
        }

        async void Start()
        {
            //escapeGameAudioPlayer.Initialize(Repository.EscapeGameAudioDataStore);
            SceneManager.sceneLoaded += SceneLoaded;
            await UniTask.Delay(10);
            ExecuteEvent(excuteEventUseCase);
            StartCoroutine(ExcuteUpdateEvent(excuteEventUseCase));

        }

        void SceneLoaded(Scene nextScene, LoadSceneMode mode)
        {
            screenEffectView.TerminateScreenEffect();
            ExecuteEvent(excuteEventUseCase);
        }

        void ExecuteEvent(IExcuteEventUseCase excuteEventUseCase)
        {
            excuteEventUseCase.ExcuteSceneLoadTimingEvent();
        }

        //UpdateEventを実行するタイムスパン
        const float UPDATE_EVENT_EXCUTE_SPAN = 1.0f;
        IEnumerator ExcuteUpdateEvent(IExcuteEventUseCase excuteEventUseCase)
        {
            while (true)
            {
                yield return new WaitForSeconds(UPDATE_EVENT_EXCUTE_SPAN);
                excuteEventUseCase.ExcuteUpdateEvent();
            }
            yield return null;
        }

        public IEscapeGameDefinsDataStore GetEscapeGameDefins()
        {
            return repository.EscapeGameDefinsDataStore;
        }

        [ContextMenu("ユーザーデータを削除")]
        void DebugDeleteUserData()
        {
            PlayerPrefs.DeleteAll();
        }


        public void ClearUserData()
        {
            repository.EscapeGameUserDataStore.ClearUserData();
        }

        public void DumpUserdata()
        {
            var data = Repository.EscapeGameUserDataStore;
            foreach (var item in data.Items)
            {
                Debug.Log(item.ItemName);
            }
            foreach (var c in data.CountEvents)
            {
                Debug.Log(c.CountEventType+":"+c.Count);
            }
            foreach (var ev in data.EventFlags)
            {
                Debug.Log(ev.EventType+":"+ ev.IsOn);
            }
        }

        public void SelectItem(string item)
        {
            itemSelectUseCase.SelectItem(item);
        }

        //TODO SynthesizeUseCase!!

        public string SynthesizeItems(string itemA, string itemB)
        {
            //まずは、合成リストにパターンマッチするものがあるかどうか調べる
            string synthesizedItemName = JugSynthesizeable(itemA,itemB);
            if (synthesizedItemName != "")
            {
                //itemAとitemBを減らす
                repository.EscapeGameUserDataStore.DecreaseItem(itemA);
                repository.EscapeGameUserDataStore.DecreaseItem(itemB);
                //合成後のアイテムを増やす
                repository.EscapeGameUserDataStore.AddItem(synthesizedItemName);
                //アイテム欄の更新
                userItemListChangeCallBack?.Invoke(repository.UserPossessionItemSpriteList);
                return synthesizedItemName;
            }
            return "";
        }

        string JugSynthesizeable(string itemA, string itemB)
        {
            foreach (var item in repository.ItemDataStore.Items)
            {
                var synthesizedItemName = item.JugSynthesizeable(itemA, itemB);
                if (synthesizedItemName != "") return synthesizedItemName;
            }
            return "";
        }

        public bool GetItemPossession(string itemName)
        {
            return repository.EscapeGameUserDataStore.InPossessionItem(itemName);
        }

        public int GetNumberEventValue(string eventName)
        {
            return repository.EscapeGameUserDataStore.GetCountEventValue(eventName);
        }

        //↓実装する

        public void GetItem(string itemName)
        {
            repository.EscapeGameUserDataStore.AddItem(itemName);
        }

        public void LostItem(string itemName)
        {
            repository.EscapeGameUserDataStore.DecreaseItem(itemName);
        }

        public void IncreaseCountEvent(string eventName, int count)
        {
            repository.EscapeGameUserDataStore.IncrementEventCount(eventName,count);
        }

        public void DecreaseCountEvent(string eventName, int count)
        {
            repository.EscapeGameUserDataStore.DecrementEventCount(eventName, count);
        }

        public void SetCountEvent(string eventName, int count)
        {
            repository.EscapeGameUserDataStore.SetEventCount(eventName, count);
        }

        [ContextMenu("カウントイベントの値をダンプ出力する")]
        void DebugDumpCountEvents()
        {
            foreach (var item in repository.EscapeGameUserDataStore.CountEvents)
            {
                Debug.Log(item.CountEventType+":"+ item.Count);
            }
        }

    }
}