﻿using System;
using System.Collections.Generic;

namespace Qitz.ADVGame
{
    public interface ICommandWrapVO
    {
        CommandHeadVO CommandHeadVO { get; }
        List<CommandVO> CommandValues { get; }
        string SelTagValue { get; }
        string BGMValue { get; }
        string BGValue { get; }
        string ActionValue { get; }
        string ImageValue { get; }
        string FilterValue { get; }
        string SEValue { get; }
        string JumpToValue { get; }
        //string ItemGetValue { get; }
        //string ItemLostValue { get; }
        //string CountUpValue { get; }
        //string CountDownValue { get; }
        //string CountSetValue { get; }
        //string TermValue { get; }
        //string TermTarget { get; }
    }
}
