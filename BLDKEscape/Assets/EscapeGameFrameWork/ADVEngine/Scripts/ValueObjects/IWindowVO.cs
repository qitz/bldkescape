﻿
namespace Qitz.ADVGame
{
    public interface IWindowVO
    {
        string WindowText { get; }
        string WindowCharacterName { get; }
        ICaracterVO WindowNaviCaracterVO { get; }

    }
}
