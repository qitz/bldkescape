﻿
namespace Qitz.ADVGame
{
    public interface IChoiceVO
    {
        string text { get; }
        string target { get; }
    }

}
