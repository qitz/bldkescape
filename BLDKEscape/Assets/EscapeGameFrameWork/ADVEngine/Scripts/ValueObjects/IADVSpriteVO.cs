﻿using UnityEngine;

namespace Qitz.ADVGame
{
    public interface IADVSpriteVO
    {
        Sprite Sprite { get; }

    }
}
