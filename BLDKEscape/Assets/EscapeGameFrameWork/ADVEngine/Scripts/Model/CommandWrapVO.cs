﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Qitz.ADVGame
{
    public class CommandHeadVO
    {
        public CommandHeadVO(CommandType commandType, string commandValue)
        {
            CommandType = commandType;
            CommandValue = commandValue;
        }
        public CommandType CommandType { get; private set; }
        public string CommandValue { get; private set; }
    }

    public class CommandWrapVO : ICommandWrapVO
    {

        public CommandWrapVO(CommandHeadVO commandHeadVO, List<CommandVO> commandValues)
        {
            this.CommandHeadVO = commandHeadVO;
            this.CommandValues = commandValues;
        }

        public CommandHeadVO CommandHeadVO { get; private set; }
        public List<CommandVO> CommandValues { get; private set; }

        public string SelTagValue => CommandHeadVO.CommandType == CommandType.SELTAG 
                                     ? CommandValues.FirstOrDefault(cd=>cd.CommandValueType== CommandValueType.TARGET).Value : "";

        public string BGMValue => CommandHeadVO.CommandType == CommandType.BGM
                                     ? CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.FILE).Value : "";
        public string ActionValue => CommandHeadVO.CommandType == CommandType.ACTION
                                     ? CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.VALUE).Value : "";
        public string BGValue => CommandHeadVO.CommandType == CommandType.BG
                                     ? CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.FILE).Value : "";

        public string JumpToValue => CommandHeadVO.CommandType == CommandType.JUMPTO
                                     ? CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.TARGET).Value : "";

        public string SEValue => CommandHeadVO.CommandType == CommandType.SE
                                     ? CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.FILE).Value : "";

        public string ImageValue => CommandHeadVO.CommandType == CommandType.SHOW_IMAGE
                                     ? CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.FILE).Value : "";

        public string FilterValue => CommandHeadVO.CommandType == CommandType.SHOW_FILTER
                                     ? CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.VALUE).Value : "";

        //public string ItemGetValue => CommandHeadVO.CommandType == CommandType.ITEMGET
        //                             ? CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.TARGET).Value : "";

        //public string ItemLostValue => CommandHeadVO.CommandType == CommandType.ITEMLOST
        //                             ? CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.TARGET).Value : "";

        //public string CountUpValue => CommandHeadVO.CommandType == CommandType.COUNTUP
        //                             ? CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.VALUE).Value : "";

        //public string CountDownValue => throw new NotImplementedException();

        //public string CountSetValue => throw new NotImplementedException();

        //public string TermValue
        //{
        //    get
        //    {
        //        if(CommandHeadVO.CommandType == CommandType.TERM && CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.VALUE) == null)
        //        {
        //            return "";
        //        }
        //        return  CommandHeadVO.CommandType == CommandType.TERM
        //                             ? CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.VALUE).Value : "";
        //    }
        //}

        //public string TermTarget => CommandHeadVO.CommandType == CommandType.TERM
        //? CommandValues.FirstOrDefault(cd => cd.CommandValueType == CommandValueType.TARGET).Value : "";

    }
    public class CommandVO
    {
        public CommandVO(CommandValueType commandValueType, string value)
        {
            CommandValueType = commandValueType;
            Value = value;
        }
        public CommandValueType CommandValueType { get; private set; }
        public string Value { get; private set; }
    }

}
