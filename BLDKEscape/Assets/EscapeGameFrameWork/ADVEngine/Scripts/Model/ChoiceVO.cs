﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Qitz.ADVGame
{
    public class ChoiceVO : IChoiceVO
    {
        public string text { get; set; }
        public string target { get; set; }
    }

}

