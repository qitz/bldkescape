﻿using System.Collections;
using System.Collections.Generic;
using Qitz.ArchitectureCore.ADVGame;
using UnityEngine;
namespace Qitz.ADVGame
{
    public class ADVSystemInitializer
    {

        static GameObject advSystem;

        [RuntimeInitializeOnLoadMethod]
        public static void ADVSystemInitialize()
        {
            advSystem = CreateSytem();
        }

        public static void ReCreateSystem()
        {
            Debug.Log("ADVReCreateSystem");
            Object.Destroy(advSystem);
            advSystem = CreateSytem();
        }

        static GameObject CreateSytem()
        {
            var ga = new GameObject();
            var controller = PrefabFolder.ResourcesLoadInstantiateTo("ADVSystem", ga.transform.parent);
            Object.Destroy(ga);
            Object.DontDestroyOnLoad(controller);
            return controller;
        }
    }
}
