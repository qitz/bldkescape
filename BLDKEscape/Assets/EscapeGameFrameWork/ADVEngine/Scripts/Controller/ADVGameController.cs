﻿
using UnityEngine;
using Qitz.ArchitectureCore.ADVGame;
using UniRx;
using UniRx.Async;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Qitz.ADVGame
{
    public interface IADVGameController
    {
        IObservable<List<int>> ASVScenarioEndObservable { get; }
        IObservable<ICutVO> ADVCutObservable { get; }
        void StartADV(string macro, Action endScenario);
        void Next(string jumpTo);
        void AddSelect(int selectNumber);
        void SetTagAction(Action<string> tagAction);
    }

    public class ADVGameController : AController<ADVGameRepository>, IADVGameController
    {
        [SerializeField]
        ADVGameRepository repository;
        [SerializeField]
        AdvView advView;
        [SerializeField]
        CanvasGroup canvasGroup;

        protected override ADVGameRepository Repository { get { return repository; } }
        Subject<ICutVO> advCutSunject = new Subject<ICutVO>();
        public IObservable<ICutVO> ADVCutObservable => advCutSunject;
        Subject<List<int>> advScenarioEndSubject = new Subject<List<int>>();
        public IObservable<List<int>> ASVScenarioEndObservable => advScenarioEndSubject;
        List<ICutVO> cutVOs => repository.CutVOs;
        int aDVCutCount => cutVOs.Count;
        int currentScenarioCutCount = 0;
        List<int> selectedChoice = new List<int>();
        string cacheMacro;
        [SerializeField]
        GameObject shield;

        void Awake()
        {
            shield.SetActive(false);
        }

        void Start()
        {
            HideADVImmidiate();
            //SetTagAction((value)=> {
            //    Debug.Log(value);
            //});
        }
        void OnDestroy()
        {
            ViewExtensions.CashClear();
        }

        public void Next(string jumpTo = "")
        {
            bool isScenarioEnd;
            if (jumpTo != "")
            {
                repository.ReLoad(cacheMacro);
                ICutVO targetCut = cutVOs.FirstOrDefault(cv => cv.SelTagValue == jumpTo);
                if (targetCut == null) throw new Exception($"jump先が存在しません:{jumpTo}");
                currentScenarioCutCount = targetCut.Number - 1;
                isScenarioEnd = ExcuteScenarioEnd(targetCut.Number);
                if (isScenarioEnd) return;
            }
            isScenarioEnd = ExcuteScenarioEnd(currentScenarioCutCount);
            if (isScenarioEnd) return;
            advCutSunject.OnNext(cutVOs[currentScenarioCutCount]);
            currentScenarioCutCount++;
        }

        bool ExcuteScenarioEnd(int _currentScenarioCutCount)
        {
            bool isScenarioEnd = cutVOs.Count <= _currentScenarioCutCount;
            if (isScenarioEnd)
            {
                advScenarioEndSubject.OnNext(selectedChoice);
                HideAdv();
            }
            return isScenarioEnd;
        }

        public void SetTagAction(Action<string> tagAction)
        {
            advView.SetTagAction(tagAction);
        }

        Action endScenario;
        public void StartADV(string macro,Action endScenario)
        {
            this.repository.Initialize(macro);
            currentScenarioCutCount = 0;
            this.endScenario = endScenario;
            this.cacheMacro = macro;
            ShowAdv();
        }

        async void ShowAdv()
        {
            //ここに排他処理を入れる
            shield.SetActive(true);
            await UniTask.Delay(550);
            //ここにフェードインも入れたい
            canvasGroup.alpha = 0;
            advView.Initialize();
            advView.gameObject.SetActive(true);
            FadeIn();
            shield.SetActive(false);
        }
        async void HideAdv()
        {
            //ここに排他処理を入れる
            shield.SetActive(true);
            await UniTask.Delay(200);
            FadeOut();
            await UniTask.Delay(1500);
            //ADVの初期化処理
            endScenario?.Invoke();
            Debug.Log("isScenarioEnd!");
            advView.gameObject.SetActive(false);
            shield.SetActive(false);
        }

        void HideADVImmidiate() {
            //ADVの初期化処理
            endScenario?.Invoke();
            Debug.Log("isScenarioEnd!");
            advView.gameObject.SetActive(false);
        }


        public void AddSelect(int selectNumber)
        {
            selectedChoice.Add(selectNumber);
        }
        void FadeIn()
        {
            // SetValue()を毎フレーム呼び出して、１秒間に０から１までの値の中間値を渡す
            iTween.ValueTo(this.gameObject, iTween.Hash("from", 0f, "to", 1f, "time", 0.4f, "onupdate", "SetCanvasGroupAlpha"));
        }
        void FadeOut()
        {
            // SetValue()を毎フレーム呼び出して、１秒間に１から０までの値の中間値を渡す
            iTween.ValueTo(this.gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 0.4f, "onupdate", "SetCanvasGroupAlpha"));
        }
        void SetCanvasGroupAlpha(float alpha)
        {
            // iTweenで呼ばれたら、受け取った値をImageのアルファ値にセット
            canvasGroup.alpha = alpha;
        }
    }
}