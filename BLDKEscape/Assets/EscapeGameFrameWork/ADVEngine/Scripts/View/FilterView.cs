﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Qitz.ADVGame
{
    public enum FilterType
    {
        NONE,
        sepia,
    }
    public class FilterView : ADVGameView
    {
        [SerializeField]
        Image sepiaImage;
        FilterType currentFilter;

        public void SetView(ICutVO vo)
        {
            var command = vo.Commands.FirstOrDefault(c => c.CommandHeadVO.CommandType == CommandType.SHOW_FILTER || c.CommandHeadVO.CommandType == CommandType.HIDE_FILTER);
            if (command != null && command.CommandHeadVO.CommandType == CommandType.SHOW_FILTER)
            {
                string value = command.FilterValue;
                FilterType fi;
                Enum.TryParse(value, out fi);
                SetFilter(fi);
                //targetImage.sprite = vo.ImageVO.SpriteBackGround;
                //targetImage.SetNativeSize();
                //FadeIn();
            }
            else if (command != null && command.CommandHeadVO.CommandType == CommandType.HIDE_FILTER)
            {
                HideFilter();
                //FadeOut();
            }
        }

        void SetFilter(FilterType filterType)
        {
            currentFilter = filterType;
            if(currentFilter == FilterType.sepia)
            {
                sepiaImage.gameObject.SetActive(true);
            }
        }
        void HideFilter()
        {
            if (currentFilter == FilterType.sepia)
            {
                sepiaImage.gameObject.SetActive(false);
            }
            currentFilter = FilterType.NONE;
        }

    }
}