﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using System.Linq;
using UniRx.Async;
using UniRx.Triggers;
using System.Linq;
using Qitz.ArchitectureCore.ADVGame;
using Qitz.EscapeFramework;

namespace Qitz.ADVGame
{
    public  abstract class  AAdvView:ADVGameView{
        public abstract void Next(string jumpTo);
        public abstract IObservable<List<int>> ASVScenarioEndObservable { get; }
        public abstract IObservable<ICutVO> ADVCutObservable { get; }
    }
    
    public class AdvView : AAdvView
    {
        [SerializeField] private ABackgroundView _backgroundView;
        [SerializeField] private AImageView _imageView;
        [SerializeField] private FilterView _filterView;
        [SerializeField] private ACharactersWrapView _charactersWrapView;
        [SerializeField] private AWindowView _windowView;
        [SerializeField] private Transform _choiceSelectViewEmitter;
        [SerializeField] private ChoiceSelectView choiceSelectViewPrefab;
        ChoiceSelectView currentChoiseSelectView;
        [SerializeField] private ADVAudioPlayer aDVAudioPlayer;
        [SerializeField] private EffectView effectView;
        public override IObservable<List<int>> ASVScenarioEndObservable => this.aDVGameController.ASVScenarioEndObservable;
        public override IObservable<ICutVO> ADVCutObservable => this.aDVGameController.ADVCutObservable;
        ICutVO currentCut;
        IDisposable cutDisposable;
        IDisposable effectDisposable;
        Action<string> tagAction;
        EscapeGameController escapeGameController;

        public async override void Next(string jumpTo = "")
        {
            if (_windowView.IsTyping)
            {
                _windowView.ShowAllText();
                return;
            }
            await this.UpdateAsObservable().Where(_ => _charactersWrapView.CharacterViews.All(cv => !cv.IsAnimating)).Take(1);
            string _jumpTo = jumpTo;
            bool ableToJump = currentCut != null && currentCut.JumpToValue != "";
            if (ableToJump) _jumpTo = currentCut.JumpToValue;
            this.aDVGameController.Next(_jumpTo);
        }

        public async void Initialize()
        {
            escapeGameController = FindObjectOfType<EscapeGameController>();
            effectView.ResetEffects();
            cutDisposable?.Dispose();
            effectDisposable?.Dispose();
            _charactersWrapView.ClearCharacterCache();
            cutDisposable = ADVCutObservable.Subscribe(cutVO => UpdateADVViews(cutVO)).AddTo(this.gameObject);
            effectDisposable = effectView.BlackOutEndObservable.Subscribe(_ => Next());
            Next();
            //ブラックアウトが走った場合は次のCutへ
        }

        public void SetTagAction(Action<string> tagAction)
        {
            this.tagAction = tagAction;
        }

        void UpdateADVViews(ICutVO cutVo)
        {
            //条件判定がある場合
            if (!ExcuteConditionJudgment(cutVo))
            {
                Next();
                return;
            }

            currentCut = cutVo;
            //ItemGetかLost
            ExcuteItemGetAndLoast(cutVo);
            //カウントイベント実行
            ExcuteCountEvents(cutVo);
            //画面エフェクトの実行
            cutVo.Commands.ForEach(cd => effectView.DoEffect(cd));
            //音楽を鳴らす
            aDVAudioPlayer.PlayAudio(cutVo.QitzAudio?.Audio);
            //seを鳴らす
            aDVAudioPlayer.PlaySE(cutVo.SE?.Audio);
            //Windowの表示更新
            _windowView.SetWindowVO(cutVo.WindowVO);
            //バックグラウンドの更新
            _backgroundView.SetBackgroundVO(cutVo.BackgroundVO);
            //イメージの更新
            _imageView.SetView(cutVo);
            //フィルター更新
            _filterView.SetView(cutVo);
            //キャラクタービューの更新
            //_charactersWrapView.SetCaracterVO(cutVo.Caracters);
            _charactersWrapView.SetView(cutVo);
            //選択肢の表示
            SetChoiceView(cutVo);
            //アクションを発火させる
            tagAction?.Invoke(cutVo.ActionValue);
        }
        //TODO UseCaseへ移管すべし
        void ExcuteItemGetAndLoast(ICutVO cutVo)
        {
            var itemGetEvents = cutVo.Commands.Where(cd => cd.CommandHeadVO.CommandType == CommandType.ITEMGET);
            foreach (var ev in itemGetEvents)
            {
                var target = ev.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.TARGET).Value;
                escapeGameController.GetItem(target);
            }
            var itemLostEvents = cutVo.Commands.Where(cd => cd.CommandHeadVO.CommandType == CommandType.ITEMLOST);
            foreach (var ev in itemLostEvents)
            {
                var target = ev.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.TARGET).Value;
                escapeGameController.LostItem(target);
            }
        }
        //TODO UseCaseへ移管すべし
        void ExcuteCountEvents(ICutVO cutVo)
        {
            var countUpEvents = cutVo.Commands.Where(cd => cd.CommandHeadVO.CommandType == CommandType.COUNTUP);
            foreach (var ce in countUpEvents)
            {
                var target = ce.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.TARGET).Value;
                var value = int.Parse(ce.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.VALUE).Value);
                escapeGameController.IncreaseCountEvent(target, value);
            }
            var countDownEvents = cutVo.Commands.Where(cd => cd.CommandHeadVO.CommandType == CommandType.COUNTDOWN);
            foreach (var ce in countDownEvents)
            {
                var target = ce.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.TARGET).Value;
                var value = int.Parse(ce.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.VALUE).Value);
                escapeGameController.DecreaseCountEvent(target, value);
            }
            var countSetEvents = cutVo.Commands.Where(cd => cd.CommandHeadVO.CommandType == CommandType.SETCOUNT);
            foreach (var ce in countSetEvents)
            {
                var target = ce.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.TARGET).Value;
                var value = int.Parse(ce.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.VALUE).Value);
                escapeGameController.SetCountEvent(target, value);
            }
        }

        //TODO UseCaseへ移管すべし
        bool ExcuteConditionJudgment(ICutVO cutVo)
        {
            bool existTermCommand = cutVo.Commands.FirstOrDefault(cd => cd.CommandHeadVO.CommandType == CommandType.TERM) != null;
            if (!existTermCommand) return true;

            //以下複数判定できるように頑張るListに入れて全てがtrueかどうか判定する
            List<bool> judgmentElementList = new List<bool>();


            //================アイテム所持判定の場合=======================
            var haveItemCommand = cutVo.Commands.FirstOrDefault(cd => cd.CommandHeadVO.CommandType == CommandType.TERM && cd.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.HAVE_ITEM) != null);
            bool existHaveItem = haveItemCommand != null;
            if (existHaveItem)
            {
                var target = haveItemCommand.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.TARGET).Value;
                //以下エスケープゲームのエンジンから対象値を見て判定する
                bool judge = escapeGameController.GetItemPossession(target);
                judgmentElementList.Add(judge);
            }
            //================アイテム非所持判定の場合=======================
            var dontHaveItemCommand = cutVo.Commands.FirstOrDefault(cd => cd.CommandHeadVO.CommandType == CommandType.TERM && cd.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.DONT_HAVE_ITEM) != null);
            bool existDontHaveItemCommand = dontHaveItemCommand != null;
            if (existDontHaveItemCommand)
            {
                var target = dontHaveItemCommand.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.TARGET).Value;
                //以下エスケープゲームのエンジンから対象値を見て判定する
                bool judge = !escapeGameController.GetItemPossession(target);
                judgmentElementList.Add(judge);
            }
            //================イベント数字が一定以上判定の場合=======================
            var moreThanCommand = cutVo.Commands.FirstOrDefault(cd => cd.CommandHeadVO.CommandType == CommandType.TERM && cd.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.MORE_THAN) != null);
            bool existMoreThanCommand = moreThanCommand != null;
            if (existMoreThanCommand)
            {
                var target = moreThanCommand.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.TARGET).Value;
                var eventValue = int.Parse(moreThanCommand.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.VALUE).Value);
                //以下エスケープゲームのエンジンから対象値を見て判定する
                bool judge = escapeGameController.GetNumberEventValue(target) >= eventValue;
                judgmentElementList.Add(judge);
            }
            //================イベント数字が一定以下の判定の場合=======================
            var lessThanCommand = cutVo.Commands.FirstOrDefault(cd => cd.CommandHeadVO.CommandType == CommandType.TERM && cd.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.LESS_THAN) != null);
            bool existLessThanCommand = lessThanCommand != null;
            if (existLessThanCommand)
            {
                var target = lessThanCommand.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.TARGET).Value;
                var eventValue = int.Parse(lessThanCommand.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.VALUE).Value);
                //以下エスケープゲームのエンジンから対象値を見て判定する
                bool judge = escapeGameController.GetNumberEventValue(target) <= eventValue;
                judgmentElementList.Add(judge);
            }
            //================イベント数字が等しい判定の場合=======================
            var equalCommand = cutVo.Commands.FirstOrDefault(cd => cd.CommandHeadVO.CommandType == CommandType.TERM && cd.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.EQUAL) != null);
            bool existEqualCommand = equalCommand != null;
            if (existEqualCommand)
            {
                var target = equalCommand.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.TARGET).Value;
                var eventValue = int.Parse(equalCommand.CommandValues.FirstOrDefault(cv => cv.CommandValueType == CommandValueType.VALUE).Value);
                //以下エスケープゲームのエンジンから対象値を見て判定する
                bool judge = escapeGameController.GetNumberEventValue(target) == eventValue;
                judgmentElementList.Add(judge);
            }

            return judgmentElementList.All(j=>j==true);
        }

        void SetChoiceView(ICutVO cutVo)
        {
            bool existSelectCommand = cutVo.Commands.FirstOrDefault(cd => cd.CommandHeadVO.CommandType == CommandType.SELECT) != null;
            if (existSelectCommand)
            {
                var salAddList = cutVo.Commands.Where(cd => cd.CommandHeadVO.CommandType == CommandType.SELADD).ToList();
                currentChoiseSelectView = ArchitectureCore.PrefabFolder.InstantiateTo<ChoiceSelectView>(choiceSelectViewPrefab,_choiceSelectViewEmitter);
                currentChoiseSelectView.Initialize(ChoiceSelectAction, salAddList);
            }

            //else
            //{
            //    _choiceSelectView.HideImmediately();
            //}
        }

        async void ChoiceSelectAction(string selectValue)
        {
            var selectItem = currentChoiseSelectView.SelectItems.FirstOrDefault(si=>si.IsSelected);
            int selectedNumber = currentChoiseSelectView.SelectItems.IndexOf(selectItem);
            this.aDVGameController.AddSelect(selectedNumber);
            await currentChoiseSelectView.HideView();
            //選択肢選択後はシナリオスクリプトをリロードする

            Next(selectValue);
        }


    }
}
