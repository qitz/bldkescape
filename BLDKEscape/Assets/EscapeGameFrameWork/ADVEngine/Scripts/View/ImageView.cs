﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Qitz.ADVGame
{
    public class ImageView : AImageView
    {
        [SerializeField]
        Image targetImage;
        [SerializeField]
        CanvasGroup canvasGroup;

        //public override void HideView()
        //{
        //    FadeOut();
        //}

        public override void SetView(ICutVO vo)
        {
            //canvasGroup.alpha = 0;

            var command = vo.Commands.FirstOrDefault(c=>c.CommandHeadVO.CommandType == CommandType.SHOW_IMAGE || c.CommandHeadVO.CommandType == CommandType.HIDE_IMAGE);

            if(command != null && command.CommandHeadVO.CommandType == CommandType.SHOW_IMAGE)
            {
                targetImage.sprite = vo.ImageVO.SpriteBackGround;
                targetImage.SetNativeSize();
                FadeIn();
            }
            else if (command != null && command.CommandHeadVO.CommandType == CommandType.HIDE_IMAGE)
            {
                FadeOut();
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            canvasGroup.alpha = 0;
        }

        //↓パフォーマンスやばそう・・・・
        void FadeIn()
        {

            // SetValue()を毎フレーム呼び出して、１秒間に０から１までの値の中間値を渡す
            iTween.ValueTo(gameObject, iTween.Hash("from", 0f, "to", 1f, "time", 1f, "onupdate", "SetAlpha"));
        }
        void FadeOut()
        {
            // SetValue()を毎フレーム呼び出して、１秒間に１から０までの値の中間値を渡す
            iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", 1f, "onupdate", "SetAlpha"));
        }
        void SetAlpha(float alpha)
        {
            // iTweenで呼ばれたら、受け取った値をImageのアルファ値にセット
            canvasGroup.alpha = alpha;
        }
    }
}