

public static class ParseCommandList
{
    public static readonly string[] characterList = new[]
    {
		"リリファ",
		"アイン",
		"二オディ",
		"シオト",
		"ローウィン",
		"ルーヤ",
		"ジグル",
		"エルバート",
		"キルエル",
        "新人栄子",
        "波話原部長",
        "平社員",
        "ハチバニ",
        "ナレーション",
    };
    
    // TODO 自動生成
    public static readonly string[] costumeList = new[]
    {
		"制服(冬服)",
		"私服(部屋着)",
		"私服(コート)",
		"冬服",
		"私服(冬服)",
		"制服(ジャージ)",
		"通常服",
    };
    
    //TODO 自動生成
    public static readonly string[] faceList = new[]
    {
		"真顔",
		"悲しみ",
		"真顔(眼鏡)",
		"半目",
		"半目(眼鏡)",
		"微笑",
		"笑顔",
		"微笑(眼鏡)",
		"驚き",
        "泣き",
        "微笑照れ",
		"半目照れ",
		"ふてくされ照れ",
		"驚き照れ",
		"真顔照れ",
		"笑顔照れ",
		"ふてくされ",
		"ときめき",
		"怒り",
		"怒り照れ",
		"笑顔(眼鏡)",
		"目閉じ",

    };
}
