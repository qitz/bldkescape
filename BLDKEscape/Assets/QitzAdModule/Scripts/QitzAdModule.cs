﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;

namespace Qitz.AdModules
{

    public interface IQitzAdModule
    {
        void ShowBanner();
        void DestroyBanner();
        void ShowInterstitial();
        void ShowReword();
        bool IsLoadedInterstitial { get; }
        bool IsLoadedReward { get; }
        void SetRewardedCallBack(Action rewardedCallBack);
    }

    public interface IAdModuleReceiver
    {
    }


    public static class AdModuleExtensions
    {

        static QitzAdModule adModule;

        public static IQitzAdModule GetQitzAdModule(this IAdModuleReceiver adModuleReceiver)
        {
            if(adModule == null){
                adModule = UnityEngine.Object.FindObjectOfType<QitzAdModule>();
            }
            return adModule;
        }

    }


    public class QitzAdModule : MonoBehaviour, IQitzAdModule
    {

        Action rewardedCallBack;

        [SerializeField]
        string androidBannerAppId = "ca-app-pub-3940256099942544~3347511713";
        [SerializeField]
        string iOSBannerAppId = "ca-app-pub-3940256099942544~3347511713";
        [SerializeField]
        string androidBannerAdUnitId = "ca-app-pub-3940256099942544/6300978111";
        [SerializeField]
        string iOSBannerAdUnitId = "ca-app-pub-3940256099942544/6300978111";


        [SerializeField]
        string androidInterstitialAdUnitId = "ca-app-pub-3940256099942544/1033173712";
        [SerializeField]
        string iOSInterstitialAdUnitId = "ca-app-pub-3940256099942544/4411468910";


        [SerializeField]
        string androidRewardAppId = "ca-app-pub-3940256099942544~3347511713";
        [SerializeField]
        string iOSRewardAppId = "ca-app-pub-3940256099942544~1458002511";
        [SerializeField]
        string androidRewardAdUnitId = "ca-app-pub-3940256099942544/5224354917";
        [SerializeField]
        string iOSRewardAdUnitId = "ca-app-pub-3940256099942544/1712485313";

        [SerializeField]
        List<string> testDevices;

        BannerView bannerView;
        InterstitialAd interstitial;
        RewardBasedVideoAd rewardBasedVideo;

        public bool IsLoadedInterstitial => interstitial.IsLoaded();
        public bool IsLoadedReward => rewardBasedVideo.IsLoaded();

        // Start is called before the first frame update
        void Start()
        {
            InitializeReword();
            InitializeInterstitial();
        }

        #region Banner


        public void ShowBanner()
        {
#if UNITY_ANDROID
            string appId = androidBannerAppId;
#elif UNITY_IPHONE
            string appId = iOSBannerAppId;
#else
            string appId = "unexpected_platform";
#endif
            // Initialize the Google Mobile Ads SDK.
            MobileAds.Initialize(appId);

            // 広告ユニットID これはテスト用
#if UNITY_ANDROID
            string adUnitId = androidBannerAdUnitId;
#elif UNITY_IPHONE
        string adUnitId = iOSBannerAdUnitId;
#else
        string adUnitId = "unexpected_platform";
#endif

            // Create a 320x50 banner at the top of the screen.
            bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

            // Create an empty ad request.
            var adBuilder = new AdRequest.Builder();
            testDevices.ForEach(id =>
            {
                adBuilder.AddTestDevice(id);
            });
            AdRequest request = adBuilder.Build();

            // Load the banner with the request.
            bannerView.LoadAd(request);

        }


        public void DestroyBanner()
        {
            this.bannerView.Destroy();
            bannerView = null;
        }

#endregion

#region Interstitial



        void InitializeInterstitial()
        {

            if(interstitial != null)
            {
                DestroyInterstitial();
            }
#if UNITY_ANDROID
            string adUnitId = androidInterstitialAdUnitId;
#elif UNITY_IPHONE
        string adUnitId = iOSInterstitialAdUnitId;
#else
        string adUnitId = "unexpected_platform";
#endif

            // Initialize an InterstitialAd.
            this.interstitial = new InterstitialAd(adUnitId);
            // Create an empty ad request.
            var adBuilder = new AdRequest.Builder();
            testDevices.ForEach(id =>
            {
                adBuilder.AddTestDevice(id);
            });
            AdRequest request = adBuilder.Build();
            // Load the interstitial with the request.
            this.interstitial.LoadAd(request);
            // Called when an ad request has successfully loaded.
            this.interstitial.OnAdLoaded += HandleOnAdLoadedInterstitial;
            // Called when an ad request failed to load.
            this.interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoadInterstitial;
            // Called when an ad is shown.
            this.interstitial.OnAdOpening += HandleOnAdOpenedInterstitial;
            // Called when the ad is closed.
            this.interstitial.OnAdClosed += HandleOnAdClosedInterstitial;
            // Called when the ad click caused the user to leave the application.
            this.interstitial.OnAdLeavingApplication += HandleOnAdLeavingApplicationInterstitial;
        }

        void HandleOnAdLoadedInterstitial(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleAdLoaded event received");
        }

        void HandleOnAdFailedToLoadInterstitial(object sender, AdFailedToLoadEventArgs args)
        {
            MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                                + args.Message);
        }

        void HandleOnAdOpenedInterstitial(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleAdOpened event received");
        }

        void HandleOnAdClosedInterstitial(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleAdClosed event received");
        }

        void HandleOnAdLeavingApplicationInterstitial(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleAdLeavingApplication event received");
        }

        public void ShowInterstitial()
        {
            if (this.interstitial.IsLoaded())
            {
                this.interstitial.Show();
            }
        }

        void DestroyInterstitial()
        {
            interstitial.Destroy();
            interstitial = null;
        }

#endregion


#region Reword



        void InitializeReword()
        {
#if UNITY_ANDROID
            string appId = androidRewardAppId;
#elif UNITY_IPHONE
            string appId = iOSRewardAppId;
#else
            string appId = "unexpected_platform";
#endif

            // Initialize the Google Mobile Ads SDK.
            MobileAds.Initialize(appId);

            // Get singleton reward based video ad reference.
            this.rewardBasedVideo = RewardBasedVideoAd.Instance;
            // Called when an ad request has successfully loaded.
            rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
            // Called when an ad request failed to load.
            rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
            // Called when an ad is shown.
            rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
            // Called when the ad starts to play.
            rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
            // Called when the user should be rewarded for watching a video.
            rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
            // Called when the ad is closed.
            rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
            // Called when the ad click caused the user to leave the application.
            rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;


            RequestRewardBasedVideo();
        }



        void RequestRewardBasedVideo()
        {
#if UNITY_ANDROID
            string adUnitId = androidRewardAdUnitId;
#elif UNITY_IPHONE
            string adUnitId = iOSRewardAdUnitId;
#else
            string adUnitId = "unexpected_platform";
#endif

            // Create an empty ad request.
            var adBuilder = new AdRequest.Builder();
            testDevices.ForEach(id =>
            {
                adBuilder.AddTestDevice(id);
            });
            AdRequest request = adBuilder.Build();
            // Load the rewarded video ad with the request.
            this.rewardBasedVideo.LoadAd(request, adUnitId);
        }
        void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
        }

        void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            MonoBehaviour.print(
                "HandleRewardBasedVideoFailedToLoad event received with message: "
                                 + args.Message);
        }

        void HandleRewardBasedVideoOpened(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
        }

        void HandleRewardBasedVideoStarted(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
        }

        void HandleRewardBasedVideoClosed(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
            this.RequestRewardBasedVideo();
        }

        void HandleRewardBasedVideoRewarded(object sender, Reward args)
        {
            string type = args.Type;
            double amount = args.Amount;
            MonoBehaviour.print(
                "HandleRewardBasedVideoRewarded event received for "
                            + amount.ToString() + " " + type);
            rewardedCallBack?.Invoke();
        }

        void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
        {
            MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
        }

        public void ShowReword()
        {
            if (rewardBasedVideo.IsLoaded())
            {
                rewardBasedVideo.Show();
            }
        }

        public void SetRewardedCallBack(Action rewardedCallBack)
        {
            this.rewardedCallBack = rewardedCallBack;
        }
        #endregion

    }
}