﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Qitz.AdModules
{
    public class AdSystemInitializer : MonoBehaviour
    {
    
        [RuntimeInitializeOnLoadMethod]
        static void GameSystemInitialize()
        {

            GameObject prefab = (GameObject)Resources.Load("QitzAdModule");
            var adModule = Instantiate(prefab, Vector3.zero, Quaternion.identity);
            DontDestroyOnLoad(adModule);

        }
    }
}
