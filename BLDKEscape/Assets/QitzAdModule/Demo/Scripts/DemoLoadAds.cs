﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qitz.AdModules;
using System;

namespace Qitz.AdModules.Demo
{
    public class DemoLoadAds : MonoBehaviour, IAdModuleReceiver, IQitzAdModule
    {
        public bool IsLoadedInterstitial => this.GetQitzAdModule().IsLoadedInterstitial;

        public bool IsLoadedReward => this.GetQitzAdModule().IsLoadedReward;

        public void DestroyBanner()
        {
            this.GetQitzAdModule().DestroyBanner();
        }

        public void SetRewardedCallBack(Action rewardedCallBack)
        {
            this.GetQitzAdModule().SetRewardedCallBack(rewardedCallBack);
        }

        public void ShowBanner()
        {
            this.GetQitzAdModule().ShowBanner();
        }

        public void ShowInterstitial()
        {
            this.GetQitzAdModule().ShowInterstitial();
        }

        public void ShowReword()
        {
            this.GetQitzAdModule().ShowReword();
        }
    }
}